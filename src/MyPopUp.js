import React from 'react';
class MyPopUp extends React.Component{
    render(){
        return(
            <div class="cover">
                <div id="order" class="MyPopUp">
                    <span class="title">{this.props.divan.title}</span>
                    <div onClick={()=>{this.props.closePopUp()}}><img class='icon' src="./assets/close_icon.png" alt="x"/></div>
                <div class='order'>
                    <div class="leftMyPop">
                    <img class='photo order_photo' src={this.props.divan.img} alt={this.props.divan.title}/>
                    <span class="price">{this.props.divan.price} руб.</span>
                    </div>  
                    <div class="vertical_line"></div>
                    <div class="comment">
                        <label for="comment">Комментарий к заказу:</label>
                        <textarea class="comment_field" id='comment' type="text" maxlength="200"></textarea>
                    </div>
                </div>
                <div class="phone">
                    <label for="phone">Ваш телефон*:</label>
                    <input id="phone" class="phone_field" onClick="value='+7'" placeholder="+7(123)4567890" type="tel" pattern="[(\+7)8][0-9]{10}"/>
                </div>
                <button class="red btn order_button">Отправить</button>
                </div>
            </div>
    )}
}
export default MyPopUp;