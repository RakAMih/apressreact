import React from 'react';
class ListProduct extends React.Component{
    render(){
        return(
        <div>
            <div  className="product">
                <img className="photo" src={this.props.divan.img} alt={this.props.divan.title}/>
                <div className="product_info">
                    <h4 className="title">{this.props.divan.title}</h4>
                    <p className="price">{this.props.divan.price} руб.</p>
                </div>
                <div className="btns">
                    <button onClick={() => {this.props.MyPopUp(this.props.divan)}} className="red btn">Заказать</button>
                    <button onClick={() => {this.props.BasketPopUp(this.props.divan)}} className="grey btn">В корзину</button>
                </div>
            </div>
        </div>
    )}
}
export default ListProduct;