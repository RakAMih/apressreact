import React from 'react';
import ListProduct from './ListProduct'
import MyPopUp from './MyPopUp'
import BasketPopUp from './BasketPopUp'
import {API} from './api/products.js'
export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      basketcover: false,
      cover: false,
      divan:''
    };
  };
  MyPopUp = (item) => {
    this.setState({cover: true, divan: item});
  };
  ClosePopUp=()=>{
    this.setState({cover: false})
  }
  BasketPopUp=(item)=>{
    this.setState({basketcover: true, divan: item})
  }
  CloseBasket=()=>{
    this.setState({basketcover: false})
  }
  render(){
    const divans = API.products;
    let order;
    if (this.state.cover){
      order = <MyPopUp
              divan={this.state.divan}
              key={this.state.divan.id}
              closePopUp={this.ClosePopUp}/>
    };
    let basket;
    if(this.state.basketcover){
      basket = <BasketPopUp
                divan={this.state.divan}
                key={this.state.divan.id}
                closeBasket={this.CloseBasket}/>
    }
    let arr = divans.map((divan)=>{
      return (
          <ListProduct
            divan={divan}
            key = {divan.id}
            MyPopUp={this.MyPopUp}
            BasketPopUp={this.BasketPopUp}
            />
      );
    });
    return(
      <div className="App">
        <div className="product-listing-wrapper">{arr}</div>
        <div>{order}</div>
        <div>{basket}</div>
      </div>
    )
  }
}
