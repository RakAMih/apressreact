import React from 'react';
export default class BasketPopUp extends React.Component{
    render(){
        return(
            <div class="BasketCover">
                <div class="BasketPopUp">
                    <div class="header">
                        <p>Вы добавили в корзину:</p>
                    </div>
                    <div id="Basket" class="cart">
                        <div onClick={()=>{this.props.closeBasket()}}><img class='icon' src='./assets/close_icon.png' alt="x"/></div>
                        <img class="photo cart_photo" src={this.props.divan.img} alt={this.props.divan.title}/>
                        <div class="cart_info">
                            <span class="title">{this.props.divan.title}</span>
                            <p class="price cart_price">{this.props.divan.price} руб.</p>
                    </div>
                </div>
            <a href="http://www.google.com" ><button class="red btn cart_button">Перейти в корзину</button></a>
        </div>
    </div>
        )
    }
}